# GitOps Training

## Welcome to the GitOps Training!
The idea of this training is to make you understand and be more familiar to the GitOps ideas and some tools.  

For that we prepared materials to present the theoretical part and concepts but also some labs to make you more confortable on executing the concepts that are covered in this training.  

To be able to follow and understand everything we assume you already have some basic knowledge of the following topics:  
- Docker :whale:
- k8s :cyclone:
- Helm :boat:
- Cloud :cloud:

If you feel comfortable about the topics above you will be totally prepared  to start learning and coding. 

So, let's do it! :sunglasses:

![Cat typing](https://media.giphy.com/media/LmNwrBhejkK9EFP504/giphy.gif)

--------------
## GitOps Concepts:

### What is GitOps:
> GitOps is a workflow that relies on software automation, to establish a desired state, which has been expressed declaratively in a version control system.


### GitOps Principles
1. Configuration for our system is described declaratively.
2. The configuration must be stored in a Version Control System.
3. Software automation rather than manual endeavour.
4. Actual state of the system monitored against the desired state. 

#### 1. Declarative Configuration :page_facing_up:
 - Allows for a definitive expression of the desired state of the system
 - Gives teams a common understanding of the intended system state
 - Provides means for recovery in teh event of disaster scenario

#### 2. Version Control :octopus:
 - Single Source of truth record the changes into application code and configuration
 - Familiar Working Practice: VCS are already use by DevOps teams
 - Rollback Mechanism: Inherent properties of VCS allow state reversion
 - Transaction Log: Inherent properties of VCS record the change  history and the actors involved 

#### 3. Automated State Change :arrows_clockwise:
 - Change should not be applied without a vetting process. So Pull or Merge requests include a Peer review and automated tests to make sure the workflow is secure.
 - The State Change Trigger can be charaterized as a Pull or Push based mechanism. 

#### 4. Observing State :eyes:
 - Making changes from outside of the workflow can lead to a mismatch between actual and desired state resulting in configuration drift
 - We need a mechanism to periodically observe the delta between actual and desired state. We can achieve this running a utility outside of the target system or where available runa an agent inside the target system. The method depends on the tool you are gonna use on your gitOps flow
 - Once a deviation is detected the solution should take one of the 2 actions:
    - Alert us to the drift so that we can make a manual repair
    - Atempt to make the repair itself in an automated fashion

--------------
## GitOps in the Wild
![Git Ops in the Wild](imgs/GitOpsWild.png)

---------------
## GitOps Push Workflow
![Git Ops Push Workflow](imgs/Push.png)

---------------
## GitOps Pull Workflow
![GitOps Pull Workflow](imgs/PULL.png)

---------------
## Differences between Push and Pull
![Differences](imgs/Differences.png)